package com.truepush.truepushlib_sdk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.TruePush;
import com.truepush.truepushsdk.TruePushCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TruePushCallBack {

    private TruePush truePush;

    String[] title;
    String spinner_item;

    SpinnerAdapter adapter;

//    private FirebaseApp firebaseApp;
//    private static final String FCM_APP_NAME = "TRUEPUSH_APP_NAME";
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = getResources().getStringArray(R.array.titles);
        adapter=new SpinnerAdapter(getApplicationContext());

        session = new Session(this);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);
                session.setFcmToken(newToken);
            }
        });


     /*   FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(task.isSuccessful()){
                    System.out.println("FCM_Token "+ task.getResult().getToken());
                    session.setFcmToken(task.getResult().getToken());
                }
            }
        });*/

        truePush = TruePush.getInstance(this, this, R.drawable.action_icon);
        System.out.println("subcriberID- "+session.getDataId());
    }

    public void notificationAllow(View view) {

        try {
            if (Build.BRAND.equalsIgnoreCase("Letv")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.letv.android.letvsafe",
                        "com.letv.android.letvsafe.AutobootManageActivity"));
                startActivity(intent);

            } else if (Build.BRAND.equalsIgnoreCase("oneplus")) {

                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                startActivity(intent);

            }else if (Build.BRAND.equalsIgnoreCase("Honor")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.huawei.systemmanager",
                        "com.huawei.systemmanager.optimize.process.ProtectActivity"));
                startActivity(intent);

            } else if (Build.MANUFACTURER.equalsIgnoreCase("oppo")) {
                Intent intent = new Intent();
                if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1){
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("android.provider.extra.APP_PACKAGE", this.getPackageName());
                }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("app_package", this.getPackageName());
                    intent.putExtra("app_uid", this.getApplicationInfo().uid);
                }else {
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + this.getPackageName()));
                }
                startActivity(intent);
            } else if (Build.MANUFACTURER.contains("vivo")) {
                Intent intent = new Intent();

                if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1){
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("android.provider.extra.APP_PACKAGE", this.getPackageName());
                }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("app_package", this.getPackageName());
                    intent.putExtra("app_uid", this.getApplicationInfo().uid);
                }else {
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + this.getPackageName()));
                }
                startActivity(intent);
            }else{
                Toast.makeText(this,"Not required for this mobile", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void makeAddSubscriberServiceCall(View view) {
        truePush.initSubscriber(this);
    }

    public void addTags(View view) {
        openDialogAddTags();
    }

    public void addUserId(View view) {
        openDialogAddUser();
    }

    public void removeTags(View view) {
        openDialogRemoveTags();
    }

    public void getTags(View view) {
        truePush.getTags(this, this);
    }

    public void getSubscriberId(View view) {
        truePush.getSubscriberId(this, this);
    }

    public void addTestUser(View view) {
        openDialogAddTestUser();
    }

    public void openDialogRemoveTags(){

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_remove);
        dialog.setCancelable(true);


        final EditText edittextName = (EditText) dialog.findViewById(R.id.editText1);
        final EditText edittextValue = (EditText) dialog.findViewById(R.id.editText2);
        Button button = (Button) dialog.findViewById(R.id.button1);


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                 dialog.dismiss();

                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("tagName", edittextName.getText().toString());
                    jsonObject.put("tagValue", edittextValue.getText().toString());
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                truePush.removeTags(MainActivity.this, MainActivity.this, jsonArray);
            }
        });
        dialog.show();

    }

    public void openDialogAddUser(){

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_remove);
        dialog.setCancelable(true);


        final EditText edittextName = (EditText) dialog.findViewById(R.id.editText1);
        final EditText edittextValue = (EditText) dialog.findViewById(R.id.editText2);
        edittextValue.setVisibility(View.GONE);
        Button button = (Button) dialog.findViewById(R.id.button1);
        button.setText("Add User");


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                truePush.addUserIds(MainActivity.this, MainActivity.this, edittextName.getText().toString());
            }
        });
        dialog.show();

    }

    public void openDialogAddTags(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_layout_add);
        dialog.setCancelable(true);

        // set the custom dialog components - text, image and button
        final Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner1);
        final EditText edittextName = (EditText) dialog.findViewById(R.id.editText1);
        final EditText edittextValue = (EditText) dialog.findViewById(R.id.editText2);
        Button button = (Button) dialog.findViewById(R.id.button1);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spinner_item = title[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                Toast.makeText(getApplicationContext(), spinner_item + " - " + edittextName.getText().toString().trim(), Toast.LENGTH_LONG).show();

                JSONArray jsonArray = new JSONArray();
                try {
                    JSONObject jsonObject = new JSONObject();
                    if(spinner_item.equalsIgnoreCase("string") ||
                            spinner_item.toString().equalsIgnoreCase("integer") ||
                            spinner_item.toString().equalsIgnoreCase("boolean")) {
                        jsonObject.put("tagName", edittextName.getText().toString());
                        jsonObject.put("tagType", spinner_item);
                        jsonObject.put("tagValue", edittextValue.getText().toString());

                        jsonArray.put(jsonObject);
                    }else{
                        jsonObject.put("tagName", edittextName.getText().toString());
                        jsonObject.put("tagType", spinner_item);

                        String[] elements = edittextValue.getText().toString().split(",");

                        List<String> fixedLenghtList = Arrays.asList(elements);
                        ArrayList<String> listOfString = new ArrayList<String>(fixedLenghtList);

                        jsonObject.put("tagValue", new JSONArray(listOfString));
                        jsonArray.put(jsonObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                truePush.addTags(MainActivity.this, MainActivity.this, jsonArray);
            }
        });
        dialog.show();
    }

    public void openDialogAddTestUser(){

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_remove);
        dialog.setCancelable(true);


        final EditText edittextName = (EditText) dialog.findViewById(R.id.editText1);
        final EditText edittextValue = (EditText) dialog.findViewById(R.id.editText2);
        edittextValue.setVisibility(View.GONE);
        Button button = (Button) dialog.findViewById(R.id.button1);
        button.setText("Add User");


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                truePush.addTestUser(MainActivity.this, MainActivity.this, edittextName.getText().toString());
            }
        });
        dialog.show();

    }

    public void updateVisit(View view) {
        truePush.appVisit(this);
    }

    @Override
    public void onSuccess(String successResponse) {

    }

    @Override
    public void onNotificationReceived(String successResponse, String fireEvent) {
        System.out.println("onNotificationReceived -Clicked");

        Intent itemintent = new Intent(getApplicationContext(), MainActivity.class);
        itemintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        JSONObject obj = null;
        try {

            obj = new JSONObject(successResponse);

        } catch (Throwable t) {
            Log.e("RSA", "Could not parse malformed JSON: \"" + successResponse + "\"");
        }


        String link = "";

        if(fireEvent != null && fireEvent.equalsIgnoreCase("action")){

            Toast.makeText(this,"notification clicked", Toast.LENGTH_SHORT).show();

            try {
                link = obj.getString("link");
                if(link != null && link.length() > 0) {
                    Intent actionIntent = new Intent(Intent.ACTION_VIEW);
                    actionIntent.setData(Uri.parse(link));
                    actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(actionIntent);
                } else{
                    startActivity(itemintent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                startActivity(itemintent);
            }


        }else if(fireEvent != null && fireEvent.equalsIgnoreCase("action0")){

            Toast.makeText(this,"button 1 clicked", Toast.LENGTH_SHORT).show();

            try {
                JSONArray arr =  new JSONArray(obj.getString("notificationActions"));
                link = arr.getJSONObject(0).getString("link");
                if(link != null && link.length() > 0) {
                    Intent actionIntent = new Intent(Intent.ACTION_VIEW);
                    actionIntent.setData(Uri.parse(link));
                    actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(actionIntent);
                } else{
                    startActivity(itemintent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                startActivity(itemintent);
            }



        } else if(fireEvent != null && fireEvent.equalsIgnoreCase("action1")){

            Toast.makeText(this,"button 2 clicked", Toast.LENGTH_SHORT).show();

            try {
                JSONArray arr =  new JSONArray(obj.getString("notificationActions"));
                link = arr.getJSONObject(1).getString("link");
                if(link != null && link.length() > 0) {
                    Intent actionIntent = new Intent(Intent.ACTION_VIEW);
                    actionIntent.setData(Uri.parse(link));
                    actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(actionIntent);
                } else{
                    startActivity(itemintent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                startActivity(itemintent);
            }
        }else if(fireEvent != null && fireEvent.equalsIgnoreCase("action2")){

            Toast.makeText(this,"button 3 clicked", Toast.LENGTH_SHORT).show();

            try {
                JSONArray arr =  new JSONArray(obj.getString("notificationActions"));
                link = arr.getJSONObject(2).getString("link");
                if(link != null && link.length() > 0) {
                    Intent actionIntent = new Intent(Intent.ACTION_VIEW);
                    actionIntent.setData(Uri.parse(link));
                    actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(actionIntent);
                } else{
                    startActivity(itemintent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                startActivity(itemintent);
            }
        }
    }

    @Override
    public void onFailure(String errorResponse) {

    }


    public class SpinnerAdapter extends BaseAdapter {
        Context context;
        private LayoutInflater mInflater;

        public SpinnerAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                v = mInflater.inflate(R.layout.row_textview, null);
                holder = new ListContent();
                holder.text = (TextView) v.findViewById(R.id.textView1);

                v.setTag(holder);
            } else {
                holder = (ListContent) v.getTag();
            }

            holder.text.setText(title[position]);

            return v;
        }
    }

    static class ListContent {
        TextView text;
    }
}

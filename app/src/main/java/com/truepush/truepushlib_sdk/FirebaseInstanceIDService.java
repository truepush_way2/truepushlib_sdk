package com.truepush.truepushlib_sdk;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.truepush.truepushsdk.Session.Session;

public class FirebaseInstanceIDService extends FirebaseMessagingService {

    Session session;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        session = new Session(this);
        Log.e("newToken", s);
        session.setFcmToken(s);
    }
}
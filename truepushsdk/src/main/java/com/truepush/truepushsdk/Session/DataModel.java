package com.truepush.truepushsdk.Session;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.Display;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;

import com.truepush.truepushsdk.models.DeviceInfo;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DataModel implements LocationListener {

    private static final long OFFSET_TIME = 2000;
    private static final float OFFSET_DISTANCE = 10;
    private double latitude;
    private double longitude;

    public void getDeviceInfo(Context context) {
        /*MessagesOrLogs.showDebugInfo("Android Secure ID : " + getAndroidSecureId(context));
        MessagesOrLogs.showDebugInfo("Serial : " + Build.SERIAL);
        MessagesOrLogs.showDebugInfo("Model : " + Build.MODEL);
        MessagesOrLogs.showDebugInfo("ID : " + Build.ID);
        MessagesOrLogs.showDebugInfo("Product : " + Build.PRODUCT);
        MessagesOrLogs.showDebugInfo("Manufacturer : " + Build.MANUFACTURER);
        MessagesOrLogs.showDebugInfo("Brand : " + Build.BRAND);
        MessagesOrLogs.showDebugInfo("Type : " + Build.TYPE);
        MessagesOrLogs.showDebugInfo("User : " + Build.USER);
        MessagesOrLogs.showDebugInfo("Base : " + Build.VERSION_CODES.BASE);
        MessagesOrLogs.showDebugInfo("Incremental : " + Build.VERSION.INCREMENTAL);
        MessagesOrLogs.showDebugInfo("SDK : " + Build.VERSION.SDK);
        MessagesOrLogs.showDebugInfo("OSApiLevel : " + Build.VERSION.SDK_INT);
        MessagesOrLogs.showDebugInfo("ProcessorVendor : " + Build.BOARD);
        MessagesOrLogs.showDebugInfo("Host : " + Build.HOST);
        MessagesOrLogs.showDebugInfo("FingerPrint : " + Build.FINGERPRINT);
        MessagesOrLogs.showDebugInfo("OSVersion : " + Build.VERSION.RELEASE);
        MessagesOrLogs.showDebugInfo("PackageName : " + context.getPackageName());
        MessagesOrLogs.showDebugInfo("PackageVersion : " + getPackageVersion(context, context.getPackageName()));
        MessagesOrLogs.showDebugInfo("ScreenResolution : " + getScreenResolution(context));
        MessagesOrLogs.showDebugInfo("InternalMemory : " + getTotalInternalMemorySize());
        MessagesOrLogs.showDebugInfo("ExternalMemory : " + getTotalExternalMemorySize());
        MessagesOrLogs.showDebugInfo("NetworkType : " + getNetworkType(context));
        MessagesOrLogs.showDebugInfo("RamMemory : " + getTotalRAM());
        MessagesOrLogs.showDebugInfo("TimeZone : " + getTimeZone());
        MessagesOrLogs.showDebugInfo("SDKVersion : " + Utils.SDK_VERSION);
        MessagesOrLogs.showDebugInfo("MobileCarrier : " + getNetworkOperator(context));
        MessagesOrLogs.showDebugInfo("PrimaryEmail : " + getPrimaryEmail(context));
*/
        Session session = new Session(context);

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceId(getAndroidSecureId(context));
        deviceInfo.setDeviceSerial(Build.SERIAL);
        deviceInfo.setDeviceModel(Build.MODEL);
        deviceInfo.setProduct(Build.PRODUCT);
        deviceInfo.setManufacturer(Build.MANUFACTURER);
        deviceInfo.setBrand(Build.BRAND);
        deviceInfo.setIncrementalVersion(Build.VERSION.INCREMENTAL);
        deviceInfo.setSdk(Build.VERSION.SDK);
        deviceInfo.setOsApiLevel(String.valueOf(Build.VERSION.SDK_INT));
        deviceInfo.setProcessorVendor(Build.BOARD);
        deviceInfo.setOsVersion(Build.VERSION.RELEASE);
        deviceInfo.setClientPackageName(context.getPackageName());
        deviceInfo.setClientPackageVersion(getPackageVersion(context, context.getPackageName()));
        deviceInfo.setScreenResolution(getScreenResolution(context));
        deviceInfo.setInternalMemory(getTotalInternalMemorySize());
        deviceInfo.setExternalMemory(getTotalExternalMemorySize());
        deviceInfo.setNetworkType(getNetworkType(context));
        deviceInfo.setRamSize(getTotalRAM());
        deviceInfo.setTimeZone(getTimeZone());
        deviceInfo.setSdkVersion(Session.SDK_VERSION);
        deviceInfo.setNetworkOperator(getNetworkOperator(context));
        deviceInfo.setPrimaryEmail(getPrimaryEmail(context));
        deviceInfo.setFcmToken(session.getFcmToken());
//        deviceInfo.setInstalledPackagesList(getInstalledPackagesList(context));
        deviceInfo.setLanguage(Locale.getDefault().getDisplayLanguage());

        session.setDeviceInfoObject(deviceInfo);
    }

    private static String getNetworkType(Context context) {
        if (isConnectedWifi(context)) {
            return "Wi-Fi";
        } else if (isConnectedMobile(context)) {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();

            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2g";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3g";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4g";
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

    private static boolean isConnectedWifi(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return (networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI);
    }

    private static boolean isConnectedMobile(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    private static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }

    private String getPackageVersion(Context context, String packageName) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void startLocationUpdates(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, OFFSET_TIME, OFFSET_DISTANCE, this);
        }
    }

    public String getInstalledPackagesList(Context context) {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> appsPackagesList = context.getPackageManager().queryIntentActivities(mainIntent, 0);
        Iterator<ResolveInfo> iteratorObject = appsPackagesList.iterator();

        StringBuilder packagesList = new StringBuilder();
        while (iteratorObject.hasNext()) {
            ResolveInfo iteratedObject = iteratorObject.next();
            //MessagesOrLogs.showDebugInfo(iteratedObject.activityInfo.toString());
            //MessagesOrLogs.showDebugInfo("packageName : " + iteratedObject.activityInfo.packageName);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                //MessagesOrLogs.showDebugInfo("parentActivityName : " + iteratedObject.activityInfo.parentActivityName);
            }
            packagesList.append(iteratedObject.activityInfo.packageName);
            packagesList.append(",");
        }

        return packagesList.toString();
    }

    public long getAppSpentTime(Context context) {
        Session session = new Session(context);
        long currentTime = System.currentTimeMillis();
        long appOpenedTime = session.getAppOpenedTime();
        return currentTime - appOpenedTime;
    }

    private String getScreenResolution(Context context) {

        WindowManager wm = (WindowManager)    context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


       /* DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;*/
        return width + "x" + height;
    }

    private static String getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        long by = (long) stat.getBlockSize() * (long) stat.getBlockCount();
        float in = by / 1048576000f;
        return (String.format(Locale.getDefault(), "%.3f GB", in));
    }

    private static String getTotalExternalMemorySize() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
            long by = (long) stat.getBlockSize() * (long) stat.getBlockCount();
            float in = by / 1048576000f;

            stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            by = (long) stat.getBlockSize() * (long) stat.getBlockCount();
            float ex = by / 1048576000f;
            if (in != ex && Math.abs(in - ex) != 0.1 && ex != 0) {
                return (String.format(Locale.getDefault(), "%.3f GB", ex));
            }
        }
        return "";
    }

    private String getTotalRAM() {
        RandomAccessFile reader;
        String load;
        DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
        double totRam;
        String lastValue = "";
        try {
            reader = new RandomAccessFile("/proc/meminfo", "r");
            load = reader.readLine();

            // Get the Number value from the string
            Pattern p = Pattern.compile("(\\d+)");
            Matcher m = p.matcher(load);
            String value = "";
            while (m.find()) {
                value = m.group(1);
            }
            reader.close();

            totRam = Double.parseDouble(value);
            // totRam = totRam / 1024;

            double mb = totRam / 1024.0;
            double gb = totRam / 1048576.0;
            double tb = totRam / 1073741824.0;

            if (tb > 1) {
                lastValue = twoDecimalForm.format(tb).concat(" TB");
            } else if (gb > 1) {
                lastValue = twoDecimalForm.format(gb).concat(" GB");
            } else if (mb > 1) {
                lastValue = twoDecimalForm.format(mb).concat(" MB");
            } else {
                lastValue = twoDecimalForm.format(totRam).concat(" KB");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            // Streams.close(reader);
        }

        return lastValue;
    }

    private String getTimeZone() {
        String timeZone;
        if (Build.VERSION.SDK_INT > 23) {
            timeZone = TimeZone.getDefault().getID();
        } else {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        return timeZone;
    }

    private String getNetworkOperator(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getNetworkOperatorName();
    }

    private String getAndroidSecureId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getPrimaryEmail(Context context) {
        String primaryEmail = "";
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+

            @SuppressLint("MissingPermission")
            Account[] accounts = AccountManager.get(context).getAccounts();

            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    primaryEmail = account.name;
                }
            }
        }
        return primaryEmail;
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

       // MessagesOrLogs.showDebugLog("Latitude : " + latitude);
       // MessagesOrLogs.showDebugLog("Longitude : " + longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //MessagesOrLogs.showDebugLog("onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        //MessagesOrLogs.showDebugLog("onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        //MessagesOrLogs.showDebugLog("onProviderDisabled");
    }
}

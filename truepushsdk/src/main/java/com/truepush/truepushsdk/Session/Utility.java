package com.truepush.truepushsdk.Session;

import android.util.Log;

public class Utility {

    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static void Log(Throwable t) {
        t.printStackTrace();
        try {
            Log.w("exception", t.getMessage());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}

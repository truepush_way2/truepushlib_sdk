package com.truepush.truepushsdk.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.truepush.truepushsdk.models.DeviceInfo;

import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class Session {

    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor sharedPreferencesEditor;

    public static final String SDK_VERSION = "0.0.178";

    private static final String FCM_TOKEN = "fcmToken";
    private static final String DEVICE_ID = "deviceId";
    private static final String APP_OPENED_TIME = "appOpenedTime";
    private static final String TRUE_PUSH_API_KEY = "truePushApiKey";
    private static final String CLASS_NAME_WITH_PACKAGE = "className";
    private static final String NOTIFICATION_DRAWABLE_ICON = "notificationDrawableIcon";
    private static final String DEVICE_INFO_OBJECT = "deviceInfoObject";
    private static final String TITLE_TO_NOTIFICATION_IDS = "titleToNotificationIds";
    private static final String IS_FCM_TOKEN_GENERATED_ON_REQUEST = "isFCMTokenGeneratedOnRequest";
    private static final String INTERNAL_NOTIFICATION_SERIAL_NUMBER = "internalNotificationSerialNumber";
    private static final String NOTIFICATION_MAIN_LINK = "notificationMainLink";
    private static final String NOTIFICATION_C_INFO = "notificationCInfo";
    private static final String UNIQUE_INT = "uniqueInt";
    private static final String DATA_ID = "dataId";
    private static final String AD_ID = "adId";
    private static final String IS_UPDATE_VISIT_CALLED = "isUpdateVisitCalled";
    private static final String IS_AD_ID_AVAILABLE = "isAdIdAvailable";
    private static final String IS_USER_DATA_CONSENT = "isUserDataConsent";
    private static final String CAMPAIGN_ID = "campaignId";
    private static final String CAMPAIGN_COUNT = "campaignCount";
    private static final String TAG_NAME = "tagName";
    private static final String TAG_VALUE = "tagValue";
    private static final String TAG_TYPE = "tagType";
    private static final String TAG_USER_ID = "tagUserId";
    private static final String IS_TAG_REMOVED = "isTagRemoved";
    private static final String SUBSCRIBER_ID = "subcriberId";


    public static final int ADD_TAGS = 1;
    public static final int REMOVE_TAGS = 2;
    public static final int GET_TAGS = 3;
    public static final int ADD_USER_ID = 4;


    public Session(Context context) {
        sharedPreferences = context.getSharedPreferences("credentialsPref", MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public String getFcmToken() {
        return sharedPreferences.getString(FCM_TOKEN, "");
    }

    public void setFcmToken(String fcmToken) {
        sharedPreferencesEditor.putString(FCM_TOKEN, fcmToken).commit();
    }

    public String getDeviceId() {
        return sharedPreferences.getString(DEVICE_ID, "");
    }

    public void setDeviceId(String deviceId) {
        sharedPreferencesEditor.putString(DEVICE_ID, deviceId).commit();
    }

    public long getAppOpenedTime() {
        return sharedPreferences.getLong(APP_OPENED_TIME, 0L);
    }

    public void setAppOpenedTime(long appOpenedTime) {
        sharedPreferencesEditor.putLong(APP_OPENED_TIME, appOpenedTime).commit();
    }

    public String getTruePushApiKey() {
        return sharedPreferences.getString(TRUE_PUSH_API_KEY, "");
    }

    public void setTruePushApiKey(String truePushApiKey) {
        sharedPreferencesEditor.putString(TRUE_PUSH_API_KEY, truePushApiKey).commit();
    }

    public String getClassNameWithPackage() {
        return sharedPreferences.getString(CLASS_NAME_WITH_PACKAGE, "");
    }

    public void setClassNameWithPackage(String className) {
        sharedPreferencesEditor.putString(CLASS_NAME_WITH_PACKAGE, className).commit();
    }

    public int getNotificationDrawableIcon() {
        return sharedPreferences.getInt(NOTIFICATION_DRAWABLE_ICON, 0);
    }

    public void setNotificationDrawableIcon(int notificationDrawableIcon) {
        sharedPreferencesEditor.putInt(NOTIFICATION_DRAWABLE_ICON, notificationDrawableIcon).commit();
    }

    public boolean isFcmTokenGeneratedOnRequest() {
        return sharedPreferences.getBoolean(IS_FCM_TOKEN_GENERATED_ON_REQUEST, false);
    }

    public void setIsFcmTokenGeneratedOnRequest(boolean isFcmTokenGeneratedOnRequest) {
        sharedPreferencesEditor.putBoolean(IS_FCM_TOKEN_GENERATED_ON_REQUEST, isFcmTokenGeneratedOnRequest).commit();
    }

    public DeviceInfo getDeviceInfoObject() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(DEVICE_INFO_OBJECT, "");
        return gson.fromJson(json, DeviceInfo.class);
    }

    public void setDeviceInfoObject(DeviceInfo deviceInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(deviceInfo);
        sharedPreferencesEditor.putString(DEVICE_INFO_OBJECT, json).commit();
    }

    public HashMap<String, Integer> getTitleToNotificationIds() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(TITLE_TO_NOTIFICATION_IDS, "");
        if (!TextUtils.isEmpty(json)) {
            return gson.fromJson(json, new TypeToken<HashMap<String, Integer>>() {
                    }.getType()
            );
        } else {
            return new HashMap<>();
        }
    }

    public void setTitleToNotificationIds(HashMap<String, Integer> titleToNotificationIds) {
        Gson gson = new Gson();
        String json = gson.toJson(titleToNotificationIds);
        sharedPreferencesEditor.putString(TITLE_TO_NOTIFICATION_IDS, json).commit();
    }

    public long getInternalNotificationSerialNumber() {
        return sharedPreferences.getLong(INTERNAL_NOTIFICATION_SERIAL_NUMBER, 0);
    }

    public void setInternalNotificationSerialNumber(long internalNotificationSerialNumber) {
        sharedPreferencesEditor.putLong(INTERNAL_NOTIFICATION_SERIAL_NUMBER, internalNotificationSerialNumber).commit();
    }

    public String getNotificationMainLink() {
        return sharedPreferences.getString(NOTIFICATION_MAIN_LINK, "");
    }

    public void setNotificationMainLink(String notificationMainLink) {
        sharedPreferencesEditor.putString(NOTIFICATION_MAIN_LINK, notificationMainLink).commit();
    }


    public String getNotificationCInfo() {
        return sharedPreferences.getString(NOTIFICATION_C_INFO, "");
    }

    public void setNotificationCInfo(String notificationCInfo) {
        sharedPreferencesEditor.putString(NOTIFICATION_C_INFO, notificationCInfo).commit();
    }

    public int getUniqueInt() {
        return sharedPreferences.getInt(UNIQUE_INT, 0);
    }

    public void setUniqueInt(int uniqueInt) {
        sharedPreferencesEditor.putInt(UNIQUE_INT, uniqueInt).commit();
    }

    public String getDataId() {
        return sharedPreferences.getString(DATA_ID, "");
    }

    public void setDataId(String dataID) {
        sharedPreferencesEditor.putString(DATA_ID, dataID).commit();
    }

    public boolean isUpdateVisitCalled() {
        return sharedPreferences.getBoolean(IS_UPDATE_VISIT_CALLED, false);
    }

    public void setIsUpdateVisitCalled(boolean isUpdateVisitCalled) {
        sharedPreferencesEditor.putBoolean(IS_UPDATE_VISIT_CALLED, isUpdateVisitCalled).commit();
    }

    public String getAdId() {
        return sharedPreferences.getString(AD_ID, "");
    }

    public void setAdId(String dataID) {
        sharedPreferencesEditor.putString(AD_ID, dataID).commit();
    }

    public boolean isAdIdAvailable() {
        return sharedPreferences.getBoolean(IS_AD_ID_AVAILABLE, false);
    }

    public void setIsAdIdAvailable(boolean isAdIdAvailable) {
        sharedPreferencesEditor.putBoolean(IS_AD_ID_AVAILABLE, isAdIdAvailable).commit();
    }

    public String getCampaignId() {
        return sharedPreferences.getString(CAMPAIGN_ID, "");
    }

    public void setCampaignId(String dataID) {
        sharedPreferencesEditor.putString(CAMPAIGN_ID, dataID).commit();
    }

    public int getCampaignCount() {
        return sharedPreferences.getInt(CAMPAIGN_COUNT, 0);
    }

    public void setCampaignCount(int dataID) {
        sharedPreferencesEditor.putInt(CAMPAIGN_COUNT, dataID).commit();
    }

    public boolean isUserDataConsent() {
        return sharedPreferences.getBoolean(IS_USER_DATA_CONSENT, false);
    }

    public void setUserDataConsent(boolean isUserDataConsent) {
        sharedPreferencesEditor.putBoolean(IS_USER_DATA_CONSENT, isUserDataConsent).commit();
    }

    public String getTagName() {
        return sharedPreferences.getString(TAG_NAME, "");
    }

    public void setTagName(String tagName) {
        sharedPreferencesEditor.putString(TAG_NAME, tagName).commit();
    }

    public String getTagValue() {
        return sharedPreferences.getString(TAG_VALUE, "");
    }

    public void setTagValue(String tagValue) {
        sharedPreferencesEditor.putString(TAG_VALUE, tagValue).commit();
    }

    public String getTagType() {
        return sharedPreferences.getString(TAG_TYPE, "");
    }

    public void setTagType(String tagType) {
        sharedPreferencesEditor.putString(TAG_TYPE, tagType).commit();
    }

    public boolean isTagRemoved() {
        return sharedPreferences.getBoolean(IS_TAG_REMOVED, false);
    }

    public void setTagRemoved(boolean isTagRemoved) {
        sharedPreferencesEditor.putBoolean(IS_TAG_REMOVED, isTagRemoved).commit();
    }

    public String getTagUserId() {
        return sharedPreferences.getString(TAG_USER_ID, "");
    }

    public void setTagUserId(String tagUserId) {
        sharedPreferencesEditor.putString(TAG_USER_ID, tagUserId).commit();
    }

    public String getSubscriberId() {
        return sharedPreferences.getString(SUBSCRIBER_ID, "");
    }

    public void setSubscriberId(String subscriberId) {
        sharedPreferencesEditor.putString(SUBSCRIBER_ID, subscriberId).commit();
    }

}

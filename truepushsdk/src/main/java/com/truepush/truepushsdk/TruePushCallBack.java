package com.truepush.truepushsdk;

public interface TruePushCallBack {

    void onSuccess(String successResponse);

    void onNotificationReceived(String successResponse, String fireEvent);

    void onFailure(String errorResponse);

}

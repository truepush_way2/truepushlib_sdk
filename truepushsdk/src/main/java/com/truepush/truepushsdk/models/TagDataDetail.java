package com.truepush.truepushsdk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagDataDetail {
    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("errorCount")
    @Expose
    private Integer errorCount;
    @SerializedName("successCount")
    @Expose
    private Integer successCount;
    @SerializedName("errors")
    @Expose
    private TagError errors;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public TagError getErrors() {
        return errors;
    }

    public void setErrors(TagError errors) {
        this.errors = errors;
    }

}

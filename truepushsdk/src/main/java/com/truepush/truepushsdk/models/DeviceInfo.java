package com.truepush.truepushsdk.models;

public class DeviceInfo {
    private String deviceId;
    private String clientPackageName;
    private String clientPackageVersion;
    private String fcmToken;
    private String networkType;
    private String deviceSerial;
    private String deviceModel;
    private String product;
    private String manufacturer;
    private String brand;
    private String processorVendor;
    private String osVersion;
    private String osApiLevel;
    private String incrementalVersion;
    private String sdk;
    private String screenResolution;
    private String internalMemory;
    private String externalMemory;
    private String ramSize;
    private String timeZone;
    private String networkOperator;
    private String primaryEmail;
    private String sdkVersion;
    private String installedPackagesList;
    private String language;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getClientPackageName() {
        return clientPackageName;
    }

    public void setClientPackageName(String clientPackageName) {
        this.clientPackageName = clientPackageName;
    }

    public String getClientPackageVersion() {
        return clientPackageVersion;
    }

    public void setClientPackageVersion(String clientPackageVersion) {
        this.clientPackageVersion = clientPackageVersion;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProcessorVendor() {
        return processorVendor;
    }

    public void setProcessorVendor(String processorVendor) {
        this.processorVendor = processorVendor;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getOsApiLevel() {
        return osApiLevel;
    }

    public void setOsApiLevel(String osApiLevel) {
        this.osApiLevel = osApiLevel;
    }

    public String getIncrementalVersion() {
        return incrementalVersion;
    }

    public void setIncrementalVersion(String incrementalVersion) {
        this.incrementalVersion = incrementalVersion;
    }

    public String getSdk() {
        return sdk;
    }

    public void setSdk(String sdk) {
        this.sdk = sdk;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

    public String getInternalMemory() {
        return internalMemory;
    }

    public void setInternalMemory(String internalMemory) {
        this.internalMemory = internalMemory;
    }

    public String getExternalMemory() {
        return externalMemory;
    }

    public void setExternalMemory(String externalMemory) {
        this.externalMemory = externalMemory;
    }

    public String getRamSize() {
        return ramSize;
    }

    public void setRamSize(String ramSize) {
        this.ramSize = ramSize;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        this.networkOperator = networkOperator;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public String getInstalledPackagesList() {
        return installedPackagesList;
    }

    public void setInstalledPackagesList(String installedPackagesList) {
        this.installedPackagesList = installedPackagesList;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "deviceId='" + deviceId + '\'' +
                ", clientPackageName='" + clientPackageName + '\'' +
                ", clientPackageVersion='" + clientPackageVersion + '\'' +
                ", fcmToken='" + fcmToken + '\'' +
                ", networkType='" + networkType + '\'' +
                ", deviceSerial='" + deviceSerial + '\'' +
                ", deviceModel='" + deviceModel + '\'' +
                ", product='" + product + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", brand='" + brand + '\'' +
                ", processorVendor='" + processorVendor + '\'' +
                ", osVersion='" + osVersion + '\'' +
                ", osApiLevel='" + osApiLevel + '\'' +
                ", incrementalVersion='" + incrementalVersion + '\'' +
                ", sdk='" + sdk + '\'' +
                ", screenResolution='" + screenResolution + '\'' +
                ", internalMemory='" + internalMemory + '\'' +
                ", externalMemory='" + externalMemory + '\'' +
                ", ramSize='" + ramSize + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", networkOperator='" + networkOperator + '\'' +
                ", primaryEmail='" + primaryEmail + '\'' +
                ", sdkVersion='" + sdkVersion + '\'' +
                ", installedPackagesList='" + installedPackagesList + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}

package com.truepush.truepushsdk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataUpdate {

    @SerializedName("serverKey")
    @Expose
    private String serverKey;
    @SerializedName("senderId")
    @Expose
    private String senderId;
    @SerializedName("pushId")
    @Expose
    private Integer pushId;

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Integer getPushId() {
        return pushId;
    }

    public void setPushId(Integer pushId) {
        this.pushId = pushId;
    }

    @Override
    public String toString() {
        return "DataUpdate{" +
                "serverKey='" + serverKey + '\'' +
                ", senderId='" + senderId + '\'' +
                ", pushId=" + pushId +
                '}';
    }
}


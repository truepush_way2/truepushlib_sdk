package com.truepush.truepushsdk.apiServiceCall;


import org.json.JSONObject;

public interface WebserviceCallback {

    void onJSONResponse(Object jsonResponse, String type);

    void onFailure(String type, Throwable error);
}
package com.truepush.truepushsdk.apiServiceCall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;

/*import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;*/
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.truepush.truepushsdk.R;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.Session.Utility;
import com.truepush.truepushsdk.models.AddUser;
import com.truepush.truepushsdk.models.GetTags;
import com.truepush.truepushsdk.models.Subcriber;
import com.truepush.truepushsdk.models.TagData;
import com.truepush.truepushsdk.models.UpdateVisit;
import com.truepush.truepushsdk.retrofitService.BaseResponseApi;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebserviceHelper {

    private Context context;
    private Session session;
    private FirebaseApp firebaseApp;
    private static final String FCM_APP_NAME = "TRUEPUSH_APP_NAME";

    public WebserviceHelper(Context context) {
        this.context = context;
        session = new Session(context);
    }

    //API Calling
    public void postDataInBody(JSONObject jsonData, final WebserviceCallback callBack, final String type) {

        RequestBody myreqbody = null;
        try {
            myreqbody = RequestBody.create((new JSONObject(String.valueOf(jsonData))).toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(isNetworkConnected()){

            Call<UpdateVisit> call = BaseResponseApi.getApiService().updateVisit(myreqbody);

            call.enqueue(new Callback<UpdateVisit>() {
                @Override
                public void onResponse(@NonNull Call<UpdateVisit> call, @NonNull Response<UpdateVisit> response) {
                    Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                    UpdateVisit resp = response.body();
                    if (resp != null) {
                          /*  try {
                              //  String senderId = resp.getData().getSenderId();
                                //Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                              *//*  if(session.getFcmToken() == null || session.getFcmToken().length() == 0) {
                                 //   System.out.println("inside FCM");
                                    AsyncTaskRunner runner = new AsyncTaskRunner();
                                    runner.execute(senderId);
                                }*//*
                            } catch (Exception t) {
                                t.printStackTrace();
                            }*/
                        try {
                            callBack.onJSONResponse(response.body().toString(), type);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
                @Override
                public void onFailure(@NonNull Call<UpdateVisit> call, @NonNull Throwable t) {
                    Utility.Log(t);
                    callBack.onFailure(type, t);
                }
            });
        }else {
            Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //API Calling
    public void postDeviceID(JSONObject jsonData, final WebserviceCallback callBack, final String type) {

        RequestBody myreqbody = null;
        try {
            myreqbody = RequestBody.create((new JSONObject(String.valueOf(jsonData))).toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(isNetworkConnected()){
            Call<Subcriber> call = BaseResponseApi.getApiService().postDeviceID(myreqbody);

            call.enqueue(new Callback<Subcriber>() {
                @Override
                public void onResponse(@NonNull Call<Subcriber> call, @NonNull Response<Subcriber> response) {
                    Subcriber resp = response.body();
                    if (resp != null) {
                     //   Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                        try {
                            callBack.onJSONResponse(response.body().toString(), type);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(@NonNull Call<Subcriber> call, @NonNull Throwable t) {
                    Utility.Log(t);
                    callBack.onFailure(type, t);
                }
            });
        }else {
            Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //API Calling
    public void getSubscriberID(JSONObject jsonData, final WebserviceCallback callBack, final String type) {

        RequestBody myreqbody = null;
        try {
            myreqbody = RequestBody.create((new JSONObject(String.valueOf(jsonData))).toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(isNetworkConnected()){
            Call<Subcriber> call = BaseResponseApi.getApiService().getSubscriber(myreqbody);

            call.enqueue(new Callback<Subcriber>() {
                @Override
                public void onResponse(@NonNull Call<Subcriber> call, @NonNull Response<Subcriber> response) {
                    Subcriber resp = response.body();
                    if (resp != null) {
                      //  Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                        try {
                            callBack.onJSONResponse(response.body().toString(), type);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(@NonNull Call<Subcriber> call, @NonNull Throwable t) {
                    Utility.Log(t);
                    callBack.onFailure(type, t);
                }
            });
        }else {
            Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //API Calling
    public void operateTags(int operateNo, JSONObject jsonData, final WebserviceCallback callBack, final String type) {

        RequestBody myreqbody = null;
        try {
            myreqbody = RequestBody.create((new JSONObject(String.valueOf(jsonData))).toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (operateNo){
            case Session.ADD_TAGS:
                if(isNetworkConnected()){
                    Call<TagData> call = BaseResponseApi.getApiService().add_tags(myreqbody);

                    call.enqueue(new Callback<TagData>() {
                        @Override
                        public void onResponse(@NonNull Call<TagData> call, @NonNull Response<TagData> response) {
                            TagData resp = response.body();
                            if (resp != null) {
                              //  Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                                try {
                                    callBack.onJSONResponse(response.body().toString(), type);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<TagData> call, @NonNull Throwable t) {
                            Utility.Log(t);
                            callBack.onFailure(type, t);
                        }
                    });
                }else {
                    Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;

            case Session.REMOVE_TAGS:

                if(isNetworkConnected()){
                    Call<TagData> remove_call = BaseResponseApi.getApiService().remove_tag(myreqbody);

                    remove_call.enqueue(new Callback<TagData>() {
                        @Override
                        public void onResponse(@NonNull Call<TagData> call, @NonNull Response<TagData> response) {
                            TagData resp = response.body();
                            if (resp != null) {
                              //  Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                                try {
                                    callBack.onJSONResponse(response.body().toString(), type);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<TagData> call, @NonNull Throwable t) {
                            Utility.Log(t);
                            callBack.onFailure(type, t);
                        }
                    });
                }else {
                    Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }


                break;

            case Session.GET_TAGS:

                if(isNetworkConnected()){
                    Call<GetTags> get_call = BaseResponseApi.getApiService().get_tag(myreqbody);

                    get_call.enqueue(new Callback<GetTags>() {
                        @Override
                        public void onResponse(@NonNull Call<GetTags> call, @NonNull Response<GetTags> response) {
                            GetTags resp = response.body();
                            if (resp != null) {
                             //   Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                                try {
                                    callBack.onJSONResponse(response.body().toString(), type);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<GetTags> call, @NonNull Throwable t) {
                            Utility.Log(t);
                            callBack.onFailure(type, t);
                        }
                    });
                }else {
                    Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;

            case Session.ADD_USER_ID:

              /*  Call<AddUser> add_user_call = BaseResponseApi.getApiService().add_user(jsonData);

                add_user_call.enqueue(new Callback<AddUser>() {
                    @Override
                    public void onResponse(@NonNull Call<AddUser> call, @NonNull Response<AddUser> response) {
                        AddUser resp = response.body();
                        if (resp != null) {
                            System.out.println("Resp add_user- " + resp.toString());
                        }
                    }
                    @Override
                    public void onFailure(@NonNull Call<AddUser> call, @NonNull Throwable t) {
                        Utility.Log(t);
                    }
                });*/

                break;
        }
    }

    //API Calling (To Be updated)
    public void updateCampaignStatus(JSONObject jsonObject, final WebserviceCallback callBack, final String type){

        RequestBody myreqbody = null;
        try {
            myreqbody = RequestBody.create((new JSONObject(String.valueOf(jsonObject))).toString(), okhttp3.MediaType.parse("application/json; charset=utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(isNetworkConnected()){
            Call<Subcriber> call = BaseResponseApi.getApiService().campaignStatus(myreqbody);

            call.enqueue(new Callback<Subcriber>() {
                @Override
                public void onResponse(@NonNull Call<Subcriber> call, @NonNull Response<Subcriber> response) {
                    Subcriber resp = response.body();
                    if (resp != null) {
                    //    Toast.makeText(context, "Api Called Successful", Toast.LENGTH_SHORT).show();
                        try {
                            callBack.onJSONResponse(resp.toString(), type);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(@NonNull Call<Subcriber> call, @NonNull Throwable t) {
                    Utility.Log(t);
                    callBack.onFailure(type, t);
                }
            });
        }else {
            Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //API Calling
    /*public void add_test_user(JSONObject jsonData) {

        if(isNetworkConnected()){
            Call<AddUser> call = BaseResponseApi.getApiService().add_test_user(jsonData);

            call.enqueue(new Callback<AddUser>() {
                @Override
                public void onResponse(@NonNull Call<AddUser> call, @NonNull Response<AddUser> response) {
                    AddUser resp = response.body();
                    if (resp != null) {
                        System.out.println("Resp- test_user" + resp.toString());
                    }
                }
                @Override
                public void onFailure(@NonNull Call<AddUser> call, @NonNull Throwable t) {
                    Utility.Log(t);
                }
            });
        }else {
            Toast.makeText(context, "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }*/


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        }
        return false;
    }

    String msg = "";
    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            try {

               /* FirebaseOptions firebaseOptions =
                        new FirebaseOptions.Builder()
                                .setGcmSenderId(params[0])
                                .setProjectId("truepushapp")
                                .setApplicationId("1:758463592994:android:0f67a82020a74eef42e87b")
                                .setApiKey(context.getString(R.string.firebase_key))
                                .build();
                firebaseApp = FirebaseApp.initializeApp(context, firebaseOptions, FCM_APP_NAME);

                msg= FirebaseInstanceId.getInstance(firebaseApp).getToken(params[0], FirebaseMessaging.INSTANCE_ID_SCOPE);

                //Updated One to Check Token Generated
                FirebaseApp.initializeApp(context);
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(task.isSuccessful()){
                            String token = task.getResult().getToken();
                            System.out.println("FCM Token- "+ token);
                        }else {
                            Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });*/
              /*  System.out.println("inside firebase option");
                FirebaseOptions firebaseOptions =
                        new FirebaseOptions.Builder()
                                //.setGcmSenderId(params[0])
                                .setProjectId("truepushapp")
                                .setApplicationId("1:758463592994:android:0f67a82020a74eef42e87b")
                                .setApiKey(session.getTruePushApiKey())
                                .build();
                firebaseApp = FirebaseApp.initializeApp(context, firebaseOptions, FCM_APP_NAME);

                msg= FirebaseInstanceId.getInstance(firebaseApp).getToken(params[0], FirebaseMessaging.INSTANCE_ID_SCOPE);

                System.out.println("FCM_Token "+ msg);*/

                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(task.isSuccessful()){
                          //  System.out.println("FCM_Token "+ task.getResult().getToken());
                            msg = task.getResult().getToken();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String result) {
           // System.out.println("FCM_Token_r3 "+ result);
            if (!TextUtils.isEmpty(result)) {
            //    System.out.println("UpdateVisit_Token "+ result);
                session.setFcmToken(result);
            }
           // new TruePush().initSubscriber(context);
        }
    }
}

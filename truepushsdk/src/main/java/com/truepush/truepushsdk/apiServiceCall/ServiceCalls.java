package com.truepush.truepushsdk.apiServiceCall;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.TruePushCallBack;
import com.truepush.truepushsdk.models.DeviceInfo;
import com.truepush.truepushsdk.retrofitService.ApiEndPoints;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCalls implements WebserviceCallback {

    private static final String SENDER_ID = "SenderId";
    private static final String DEVICE_INFO = "DeviceInfo";
    private static final String ADD_TAGS = "addTags";
    private static final String REMOVE_TAGS = "removeTags";
    private static final String GET_TAGS = "getTags";
    private static final String UPDATE_CAMPAIGN_STATS = "updateCampaignStats";
    private static final String UPDATE_VISIT_STATS = "updateVisit";
    private static final String ADD_TEST_USER = "addTestUser";
    private static final String GET_SUBSCRIBER_ID = "getSubscriberId";
    public static TruePushCallBack truePushCallBacks;
    private Context context;
    //   private FirebaseApp firebaseApp;
//    private static final String FCM_APP_NAME = "TRUEPUSH_APP_NAME";
    private Session session;


    public void webServiceCallToUpdateVisitStats(Context context) {
        this.context = context;
        session = new Session(context);
        DeviceInfo deviceInfo = session.getDeviceInfoObject();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceId", deviceInfo.getDeviceId());
            jsonParams.put("clientPackageName", context.getPackageName());
            jsonParams.put("fcmToken", session.getFcmToken());
            jsonParams.put("platformId", session.getTruePushApiKey());
            jsonParams.put("visit", true);
            jsonParams.put("clientPackageName", deviceInfo.getClientPackageName());
            jsonParams.put("truePushApiKey", session.getTruePushApiKey());
            jsonParams.put("deniedPersonalData", session.isUserDataConsent());
            jsonParams.put("sdkVersion", Session.SDK_VERSION);
            System.out.println("FCM_SP " + session.getFcmToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject deviceInfoParams = new JSONObject();
//            deviceInfoParams.put("primaryEmail", deviceInfo.getPrimaryEmail());
            deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
            deviceInfoParams.put("clientPackageVersion", deviceInfo.getClientPackageVersion());
            deviceInfoParams.put("networkType", deviceInfo.getNetworkType());
            deviceInfoParams.put("brand", deviceInfo.getBrand());
            deviceInfoParams.put("deviceModel", deviceInfo.getDeviceModel());
            deviceInfoParams.put("deviceSerial", deviceInfo.getDeviceSerial());
            deviceInfoParams.put("internalMemory", deviceInfo.getInternalMemory());
            deviceInfoParams.put("externalMemory", deviceInfo.getExternalMemory());
            deviceInfoParams.put("incrementalVersion", deviceInfo.getIncrementalVersion());
            deviceInfoParams.put("manufacturer", deviceInfo.getManufacturer());
            deviceInfoParams.put("networkOperator", deviceInfo.getNetworkOperator());
            deviceInfoParams.put("osApiLevel", deviceInfo.getOsApiLevel());
            deviceInfoParams.put("osVersion", deviceInfo.getOsVersion());
            deviceInfoParams.put("processorVendor", deviceInfo.getProcessorVendor());
            deviceInfoParams.put("product", deviceInfo.getProduct());
            deviceInfoParams.put("ramSize", deviceInfo.getRamSize());
            deviceInfoParams.put("screenResolution", deviceInfo.getScreenResolution());
            deviceInfoParams.put("sdk", deviceInfo.getSdk());
            deviceInfoParams.put("timeZone", deviceInfo.getTimeZone());
            deviceInfoParams.put("language", deviceInfo.getLanguage());
            deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
            deviceInfoParams.put("adId", session.getAdId());
            deviceInfoParams.put("serviceVersion", ApiEndPoints.SERVICE_VERSION);

            jsonParams.put("deviceInfo", deviceInfoParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //new WebserviceHelper(context).postDataInBody(ApiEndPoints.UPDATE_VISIT_STATS, UPDATE_VISIT_STATS, this, jsonParams);
        new WebserviceHelper(context).postDataInBody(jsonParams, this, UPDATE_VISIT_STATS);
    }


    public void webServiceCallToPostDeviceInfo(Context context, TruePushCallBack truePushCallBacks) {
        this.context = context;
        session = new Session(context);

        ServiceCalls.Client client = new ServiceCalls.Client();
        client.registerClient(truePushCallBacks);

        DeviceInfo deviceInfo = session.getDeviceInfoObject();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceId", deviceInfo.getDeviceId());
            jsonParams.put("clientPackageName", deviceInfo.getClientPackageName());
            jsonParams.put("fcmToken", session.getFcmToken());
//            jsonParams.put("primaryEmail", deviceInfo.getPrimaryEmail());
            jsonParams.put("truePushApiKey", session.getTruePushApiKey());
            jsonParams.put("sdkVersion", Session.SDK_VERSION);
            jsonParams.put("deniedPersonalData", session.isUserDataConsent());
//            jsonParams.put("installedPackages", deviceInfo.getInstalledPackagesList());

            JSONObject deviceInfoParams = new JSONObject();
            deviceInfoParams.put("clientPackageVersion", deviceInfo.getClientPackageVersion());
            deviceInfoParams.put("networkType", deviceInfo.getNetworkType());
            deviceInfoParams.put("brand", deviceInfo.getBrand());
            deviceInfoParams.put("deviceModel", deviceInfo.getDeviceModel());
            deviceInfoParams.put("deviceSerial", deviceInfo.getDeviceSerial());
            deviceInfoParams.put("internalMemory", deviceInfo.getInternalMemory());
            deviceInfoParams.put("externalMemory", deviceInfo.getExternalMemory());
            deviceInfoParams.put("incrementalVersion", deviceInfo.getIncrementalVersion());
            deviceInfoParams.put("manufacturer", deviceInfo.getManufacturer());
            deviceInfoParams.put("networkOperator", deviceInfo.getNetworkOperator());
            deviceInfoParams.put("osApiLevel", deviceInfo.getOsApiLevel());
            deviceInfoParams.put("osVersion", deviceInfo.getOsVersion());
            deviceInfoParams.put("processorVendor", deviceInfo.getProcessorVendor());
            deviceInfoParams.put("product", deviceInfo.getProduct());
            deviceInfoParams.put("ramSize", deviceInfo.getRamSize());
            deviceInfoParams.put("screenResolution", deviceInfo.getScreenResolution());
            deviceInfoParams.put("sdk", deviceInfo.getSdk());
            deviceInfoParams.put("timeZone", deviceInfo.getTimeZone());
            deviceInfoParams.put("language", deviceInfo.getLanguage());
            deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
            deviceInfoParams.put("adId", session.getAdId());
            deviceInfoParams.put("serviceVersion", ApiEndPoints.SERVICE_VERSION);

            jsonParams.put("deviceInfo", deviceInfoParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //new WebserviceHelper(context).postDataInBody(Services.POST_DEVICE_INFO, DEVICE_INFO, this, jsonParams);
        new WebserviceHelper(context).postDeviceID(jsonParams, this, DEVICE_INFO);
    }


    public void webServiceCallToGetSubscriberId(Context context, TruePushCallBack truePushCallBacks) {
        this.context = context;
        session = new Session(context);

        ServiceCalls.Client client = new ServiceCalls.Client();
        client.registerClient(truePushCallBacks);

        if (session.getDataId() != null && session.getDataId().length() > 0) {
            if (truePushCallBacks != null)
                truePushCallBacks.onSuccess(session.getDataId());
        } else {
            DeviceInfo deviceInfo = session.getDeviceInfoObject();

            JSONObject jsonParams = new JSONObject();
            try {
                System.out.println("package- " + deviceInfo.getClientPackageName());
                jsonParams.put("clientPackageName", deviceInfo.getClientPackageName());
                jsonParams.put("truePushApiKey", session.getTruePushApiKey());
                jsonParams.put("deviceId", deviceInfo.getDeviceId());
                jsonParams.put("fcmToken", session.getFcmToken());
                jsonParams.put("sdkVersion", Session.SDK_VERSION);

                JSONObject deviceInfoParams = new JSONObject();
                deviceInfoParams.put("clientPackageVersion", deviceInfo.getClientPackageVersion());
                deviceInfoParams.put("networkType", deviceInfo.getNetworkType());
                deviceInfoParams.put("brand", deviceInfo.getBrand());
                deviceInfoParams.put("deviceModel", deviceInfo.getDeviceModel());
                deviceInfoParams.put("deviceSerial", deviceInfo.getDeviceSerial());
                deviceInfoParams.put("internalMemory", deviceInfo.getInternalMemory());
                deviceInfoParams.put("externalMemory", deviceInfo.getExternalMemory());
                deviceInfoParams.put("incrementalVersion", deviceInfo.getIncrementalVersion());
                deviceInfoParams.put("manufacturer", deviceInfo.getManufacturer());
                deviceInfoParams.put("networkOperator", deviceInfo.getNetworkOperator());
                deviceInfoParams.put("osApiLevel", deviceInfo.getOsApiLevel());
                deviceInfoParams.put("osVersion", deviceInfo.getOsVersion());
                deviceInfoParams.put("processorVendor", deviceInfo.getProcessorVendor());
                deviceInfoParams.put("product", deviceInfo.getProduct());
                deviceInfoParams.put("ramSize", deviceInfo.getRamSize());
                deviceInfoParams.put("screenResolution", deviceInfo.getScreenResolution());
                deviceInfoParams.put("sdk", deviceInfo.getSdk());
                deviceInfoParams.put("timeZone", deviceInfo.getTimeZone());
                deviceInfoParams.put("language", deviceInfo.getLanguage());
                deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
                deviceInfoParams.put("adId", session.getAdId());
                deviceInfoParams.put("serviceVersion", ApiEndPoints.SERVICE_VERSION);

                jsonParams.put("deviceInfo", deviceInfoParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            new WebserviceHelper(context).getSubscriberID(jsonParams, this, GET_SUBSCRIBER_ID);
        }
    }


    public void webServiceCallForTagsManipulation(Context context, Integer manipulationType, Object tagsData, TruePushCallBack truePushCallBacks) {

        session = new Session(context);
        DeviceInfo deviceInfo = session.getDeviceInfoObject();

        ServiceCalls.Client client = new ServiceCalls.Client();
        client.registerClient(truePushCallBacks);

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceId", deviceInfo.getDeviceId());
            jsonParams.put("clientPackageName", context.getPackageName());
            jsonParams.put("fcmToken", session.getFcmToken());
            jsonParams.put("truePushApiKey", session.getTruePushApiKey());
            jsonParams.put("sdkVersion", Session.SDK_VERSION);

            if (manipulationType == Session.ADD_TAGS || manipulationType == Session.REMOVE_TAGS) {
                jsonParams.put("tags", tagsData);
            }

            if (manipulationType == Session.ADD_USER_ID) {
                jsonParams.put("userId", tagsData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (manipulationType) {
            case Session.ADD_TAGS:
                new WebserviceHelper(context).operateTags(Session.ADD_TAGS, jsonParams, this, ADD_TAGS);
                break;

            case Session.REMOVE_TAGS:
                new WebserviceHelper(context).operateTags(Session.REMOVE_TAGS, jsonParams, this, REMOVE_TAGS);
                break;

            case Session.GET_TAGS:
                new WebserviceHelper(context).operateTags(Session.GET_TAGS, jsonParams, this, GET_TAGS);
                break;

            case Session.ADD_USER_ID:
                new WebserviceHelper(context).operateTags(Session.ADD_USER_ID, jsonParams, this, ADD_TEST_USER);
                break;
        }
    }


    public void webServiceCallToAddTestUser(Context context, String name) {
        this.context = context;
        session = new Session(context);

        DeviceInfo deviceInfo = session.getDeviceInfoObject();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("clientPackageName", deviceInfo.getClientPackageName());
            jsonParams.put("truePushApiKey", session.getTruePushApiKey());
            jsonParams.put("deviceId", deviceInfo.getDeviceId());
            jsonParams.put("fcmToken", session.getFcmToken());
            jsonParams.put("userName", name);
            jsonParams.put("sdkVersion", Session.SDK_VERSION);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //new WebserviceHelper(context).add_test_user(jsonParams);
    }


    public void webServiceCallToUpdateCampaignStats(Context contex, JSONObject campaignData, String event, String eventType) {
        this.context = contex;
        session = new Session(context);
        DeviceInfo deviceInfo = session.getDeviceInfoObject();

        try {
            if (campaignData != null) {
                campaignData.put("userPlatformId", session.getTruePushApiKey());
                campaignData.put("subscriberId", session.getDataId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("deviceId", deviceInfo.getDeviceId());
            jsonParams.put("clientPackageName", context.getPackageName());
            jsonParams.put("fcmToken", session.getFcmToken());
            jsonParams.put("truePushApiKey", session.getTruePushApiKey());
            jsonParams.put("campaignData", campaignData);
            jsonParams.put("event", event);
            jsonParams.put("eventType", eventType);
            jsonParams.put("subscriberId", session.getDataId());
            jsonParams.put("provideUserConsent", session.isUserDataConsent());
            jsonParams.put("deniedPersonalData", session.isUserDataConsent());
            jsonParams.put("sdkVersion", Session.SDK_VERSION);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject deviceInfoParams = new JSONObject();
//            deviceInfoParams.put("primaryEmail", deviceInfo.getPrimaryEmail());
            deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
            deviceInfoParams.put("clientPackageVersion", deviceInfo.getClientPackageVersion());
            deviceInfoParams.put("networkType", deviceInfo.getNetworkType());
            deviceInfoParams.put("brand", deviceInfo.getBrand());
            deviceInfoParams.put("deviceModel", deviceInfo.getDeviceModel());
            deviceInfoParams.put("deviceSerial", deviceInfo.getDeviceSerial());
            deviceInfoParams.put("internalMemory", deviceInfo.getInternalMemory());
            deviceInfoParams.put("externalMemory", deviceInfo.getExternalMemory());
            deviceInfoParams.put("incrementalVersion", deviceInfo.getIncrementalVersion());
            deviceInfoParams.put("manufacturer", deviceInfo.getManufacturer());
            deviceInfoParams.put("networkOperator", deviceInfo.getNetworkOperator());
            deviceInfoParams.put("osApiLevel", deviceInfo.getOsApiLevel());
            deviceInfoParams.put("osVersion", deviceInfo.getOsVersion());
            deviceInfoParams.put("processorVendor", deviceInfo.getProcessorVendor());
            deviceInfoParams.put("product", deviceInfo.getProduct());
            deviceInfoParams.put("ramSize", deviceInfo.getRamSize());
            deviceInfoParams.put("screenResolution", deviceInfo.getScreenResolution());
            deviceInfoParams.put("sdk", deviceInfo.getSdk());
            deviceInfoParams.put("timeZone", deviceInfo.getTimeZone());
            deviceInfoParams.put("language", deviceInfo.getLanguage());
            deviceInfoParams.put("sdkVersion", deviceInfo.getSdkVersion());
            deviceInfoParams.put("adId", session.getAdId());
            deviceInfoParams.put("serviceVersion", ApiEndPoints.SERVICE_VERSION);


            jsonParams.put("deviceInfo", deviceInfoParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new WebserviceHelper(context).updateCampaignStatus(jsonParams, this, UPDATE_CAMPAIGN_STATS);
    }

    public static class Client {
        public void registerClient(TruePushCallBack truePushCallBacks) {
            ServiceCalls.truePushCallBacks = truePushCallBacks;
        }
    }

    @Override
    public void onJSONResponse(Object jsonResponse, String type) {
        switch (type) {
            case DEVICE_INFO:
                if (jsonResponse != null) {
                  //  JSONObject obj = null;
                    try {
                        // obj = new JSONObject(jsonResponse);
                        try {
                            Gson gson = new Gson();
                            String jsonString = gson.toJson(jsonResponse);
                            JSONObject jsonObject = new JSONObject(jsonString);

                            if (jsonObject.has("data")) {
                                String dataId = jsonObject.getString("data");
                                new Session(context).setDataId(dataId);
                            }
                            if (jsonObject.getString("status_code").equalsIgnoreCase("SUCCESS") &&
                                    !new Session(context).isUpdateVisitCalled()) {

                                webServiceCallToUpdateVisitStats(context);
                                new Session(context).setIsUpdateVisitCalled(true);
                            }

                            if (truePushCallBacks != null)
                                truePushCallBacks.onSuccess(jsonString);
                        } catch (JSONException err) {
                            Log.d("Error", err.toString());
                        }


                    } catch (Throwable t) {
                        t.fillInStackTrace();
                    }
                }
                break;

            case ADD_TAGS:
            case REMOVE_TAGS:
            case GET_TAGS:
                Gson gson = new Gson();
                String jsonString = gson.toJson(jsonResponse);
                if (truePushCallBacks != null)
                    truePushCallBacks.onSuccess(jsonString);
                break;

            case UPDATE_CAMPAIGN_STATS:
            case UPDATE_VISIT_STATS:
                /*if(session.getFcmToken() == null || session.getFcmToken().length() == 0) {
                    AsyncTaskRunner runner = new AsyncTaskRunner();
                    runner.execute(senderId);
                }*/
                if (jsonResponse != null) {
                    //JSONObject obj = null;
                    try {

                       /* Gson gson_ = new Gson();
                        String jsonString_ = gson_.toJson(jsonResponse);
                        JSONObject jsonObject = new JSONObject(jsonString_);
                        String senderId = jsonObject.getJSONObject("data").getString("senderId");*/

                        if (session.getFcmToken() == null || session.getFcmToken().length() == 0) {
                            //AsyncTaskRunner runner = new AsyncTaskRunner();
                            //runner.execute(senderId);
                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (task.isSuccessful()) {
                                        //System.out.println("FCM_Token "+ task.getResult().getToken());
                                        session.setFcmToken(task.getResult().getToken());
                                    }
                                }
                            });
                        }
                    } catch (Exception t) {
                        t.printStackTrace();
                    }
                }
                break;

            case SENDER_ID:
                if (jsonResponse != null) {

                    JSONObject obj = null;
                    try {
                    //   Gson gson_ = new Gson();
                    //    String jsonString_ = gson_.toJson(jsonResponse);
                    //    obj = new JSONObject(jsonString_);
                     //   String senderId = obj.getJSONObject("data").getJSONObject("fcmDetails").getString("senderId");

                        if (new Session(context).getFcmToken() == null || new Session(context).getFcmToken().length() == 0) {
                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (task.isSuccessful()) {
                                       // System.out.println("FCM_Token " + task.getResult().getToken());
                                        session.setFcmToken(task.getResult().getToken());
                                    }
                                }
                            });
                        }
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
                break;

            case GET_SUBSCRIBER_ID:

                if (jsonResponse != null) {

                    JSONObject obj = null;
                    try {

                        try {
                            Gson gson_ = new Gson();
                            String jsonString_ = gson_.toJson(jsonResponse);
                            obj = new JSONObject(jsonString_);

                            if (obj.has("data")) {
                                String dataId = obj.getString("data");

                                new Session(context).setDataId(dataId);
                               // Toast.makeText(context, "subscriberID Successfully save", Toast.LENGTH_SHORT).show();
                                if (truePushCallBacks != null)
                                    truePushCallBacks.onSuccess(dataId);
                            }

                        } catch (JSONException err) {
                            Log.d("Error", err.toString());
                        }

                    } catch (Exception t) {
                        t.printStackTrace();
                    }

                }
                break;
        }
    }

    @Override
    public void onFailure(String type, Throwable error) {
        if (truePushCallBacks != null)
            truePushCallBacks.onFailure("" + error);
    }
}

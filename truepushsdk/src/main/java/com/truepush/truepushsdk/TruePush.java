package com.truepush.truepushsdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.truepush.truepushsdk.Session.DataModel;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.apiServiceCall.ServiceCalls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class TruePush {

    private static TruePush truePushInstance;
    private static ServiceCalls serviceCalls;
    private static String advertiserId;
    public static TruePushCallBack truePushCallbacksInstance;
    private static TruePushCallBack truePushCallBacks;

    public static TruePush getInstance(final Context context, TruePushCallBack truePushCallbacks,  int app_logo) {

        truePushCallbacksInstance = truePushCallbacks;
        if (truePushInstance == null) {
            truePushInstance = new TruePush();
        }

        final Session session = new Session(context);
        session.setNotificationDrawableIcon(app_logo);

        serviceCalls = new ServiceCalls();

        ApplicationInfo ai;
        try {
            ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String truePushApiKey = bundle.getString("true_push_api_key");
            session.setTruePushApiKey(truePushApiKey);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        DataModel dataModel = new DataModel();
        dataModel.getDeviceInfo(context);

        if(!session.isAdIdAvailable()) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        AdvertisingIdClient.Info id = AdvertisingIdClient.getAdvertisingIdInfo(context);
                        if (id != null)
                            advertiserId = (id.getId());
                        else if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
                            id = AdvertisingIdClient.getAdvertisingIdInfo(context);
                            if (id != null)
                                advertiserId = (id.getId());
                        }
                        if (advertiserId != null && !advertiserId.equalsIgnoreCase("")) {
                            session.setAdId(advertiserId);
//                            serviceCalls.webServiceCallToGetSenderId(context);
                            serviceCalls.webServiceCallToUpdateVisitStats(context);
                            session.setIsAdIdAvailable(true);
                        }
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
//                    Log.e(TAG,"GooglePlayServicesNotAvailableException"+((e != null) ? e.getMessage() + "" : ""));
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
//                    Log.e(TAG,"GooglePlayServicesRepairableException"+((e != null) ? e.getMessage() + "" : ""));
                    } catch (IOException e) {
                        e.printStackTrace();
//                    Log.e(TAG,"IOException"+((e != null) ? e.getMessage() + "" : ""));
                    } catch (Exception e) {
//                    Log.e(TAG,"Exception"+((e != null) ? e.getMessage() + "" : ""));
                        e.printStackTrace();
                    }
                }
            });

        } else {
//            serviceCalls.webServiceCallToGetSenderId(context);
            serviceCalls.webServiceCallToUpdateVisitStats(context);
        }

        return truePushInstance;

    }

    public void initSubscriber(Context context) {
        serviceCalls.webServiceCallToPostDeviceInfo(context, truePushCallBacks);
    }

    public void addTags(Context context, TruePushCallBack truePushCallbacks, Object tagsData) {
        Session session = new Session(context);
        String tagName = "", tagValue = "", tagType = "";
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(tagsData.toString());
//            for(int n = 0; n < jsonArray.length(); n++)
//            {
            JSONObject object = jsonArray.getJSONObject(0);
            tagName = object.getString("tagName");
            tagValue = object.getString("tagValue");
            tagType = object.getString("tagType");
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(tagType.equals(session.getTagType()) && tagName.equals(session.getTagName()) &&
                tagValue.equals(session.getTagValue())){
            if(session.isTagRemoved()){
                session.setTagName(tagName);
                session.setTagValue(tagValue);
                session.setTagType(tagType);
                serviceCalls.webServiceCallForTagsManipulation(context, Session.ADD_TAGS, tagsData, truePushCallbacks);
            }else{
                if (truePushCallBacks != null)
                    truePushCallBacks.onSuccess("TAG already added");
            }
        } else {
            session.setTagName(tagName);
            session.setTagValue(tagValue);
            session.setTagType(tagType);
            serviceCalls.webServiceCallForTagsManipulation(context, Session.ADD_TAGS, tagsData, truePushCallbacks);
        }
//        serviceCalls.webServiceCallForTagsManipulation(context, truePushCallbacks, Utils.ADD_TAGS, tagsData);
    }

    public void addUserIds(Context context, TruePushCallBack truePushCallbacks, Object userId) {
        Session session = new Session(context);
        if(!session.getTagUserId().equals(userId.toString())){
            session.setTagUserId(userId.toString());
            serviceCalls.webServiceCallForTagsManipulation(context, Session.ADD_USER_ID, userId, truePushCallbacks);
        } else {
            if (truePushCallBacks != null)
                truePushCallBacks.onSuccess("USER already added");
        }
    }

    public void removeTags(Context context, TruePushCallBack truePushCallbacks, Object tagsData) {
        Session session = new Session(context);
        session.setTagRemoved(true);
        serviceCalls.webServiceCallForTagsManipulation(context, Session.REMOVE_TAGS, tagsData, truePushCallbacks);
    }

    public void getTags(Context context, TruePushCallBack truePushCallBacks) {
        serviceCalls.webServiceCallForTagsManipulation(context, Session.GET_TAGS, null, truePushCallBacks);
    }

    public void appVisit(Context context) {
        serviceCalls.webServiceCallToUpdateVisitStats(context);
    }

    public void addTestUser(Context context, TruePushCallBack truePushCallbacks, String name) {
        serviceCalls.webServiceCallToAddTestUser(context, name);

    }

    public void getSubscriberId(Context context, TruePushCallBack truePushCallBacks) {
        serviceCalls.webServiceCallToGetSubscriberId(context, truePushCallBacks);
    }

}

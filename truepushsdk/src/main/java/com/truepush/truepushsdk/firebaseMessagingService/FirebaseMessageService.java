package com.truepush.truepushsdk.firebaseMessagingService;

import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


import com.truepush.truepushsdk.R;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.Session.Utility;
import com.truepush.truepushsdk.TruePushCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.truepush.truepushsdk.TruePush.truePushCallbacksInstance;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class FirebaseMessageService extends BroadcastReceiver {

    private static String TAG = "FirebaseMessage";
    private boolean jobCancelled = false;

    private NotificationUtils notificationUtils;
    private List<NotificationCompat.Action> notificationActionsList;
    private int notificationId;
    private Session session;
    private String link, campaignDataInfo;
//    public static TruePushCallBack truePushCallbacksInstance;

    @Override
    public void onReceive(Context context, Intent intent) {

        session = new Session(context);

        if(!NotificationManagerCompat.from(context).areNotificationsEnabled()){
            return;
        }

        doInBackground(context, intent);
    }

   /* @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "onStartJob");
        context = this;
        doInBackground(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        jobCancelled = true;
        return true;
    }*/

    private void doInBackground(final Context context, final Intent parameters){
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {

                    if(jobCancelled){
                        return;
                    }

                    String remoteMessage = parameters.getExtras().getString("notificationData");

                    if (remoteMessage == null)
                        return;


                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(remoteMessage);
                        JSONObject jsonObjectNotificationInfo = jsonObject.getJSONObject("notificationInfo");
                        JSONObject jsonObjectCampaign = jsonObject.getJSONObject("campaignInfo");


                        if(session.getCampaignId().contains(jsonObjectCampaign.getString("campaignId"))){
                            System.out.println(("duplicate 1 : "+ session.getCampaignId()));
                            return;
                        } else {
                            System.out.println("duplicate 2 : "+session.getCampaignId());

                            if(session.getCampaignCount() == 400){
                                String substring = session.getCampaignId().substring(0,session.getCampaignId().indexOf('@')+1);
                                System.out.println("duplicate 2.1 : "+session.getCampaignId());
                                System.out.println("duplicate 2.2 : "+substring);
                                session.setCampaignId(session.getCampaignId().replace(substring, ""));
                                session.setCampaignId(session.getCampaignId()+jsonObjectCampaign.getString("campaignId")+"@");
                                System.out.println("duplicate 3 : "+session.getCampaignId());
                            } else {
                                session.setCampaignCount(session.getCampaignCount()+1);
                                session.setCampaignId(session.getCampaignId()+jsonObjectCampaign.getString("campaignId")+"@");
                                System.out.println("duplicate 4 : "+session.getCampaignId());
                            }
                        }

                        handleNotification(context, remoteMessage);

                        // Check if message contains a data payload.
                        if (remoteMessage.length() > 0) {
                            System.out.println("Data: " + remoteMessage);

                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    handleDataMessage(context,remoteMessage);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    //Finishing it manually
                    //jobFinished(parameters, false);
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void handleNotification(Context context, String message) {
        if (!NotificationUtils.isAppIsInBackground(context.getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Utility.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
         //   LocalBroadcastManager.getInstance(context).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(context.getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handleDataMessage(Context context, String json) {
        System.out.println("push json: " + json.toString());

        try {
//            String notificationData = json.getString("notificationData");
           // System.out.println("notificationInfo======>" + json);

            JSONObject jsonObject = new JSONObject(json);
            JSONObject jsonObjectNotificationInfo = jsonObject.getJSONObject("notificationInfo");
            JSONObject jsonObjectCampaign = jsonObject.getJSONObject("campaignInfo");

//            MessagesOrLogs.showDebugLog("decodedBytes======>" + decodedBytes);
//            MessagesOrLogs.showDebugLog("jsonObjectNotificationInfo======>" + jsonObjectNotificationInfo);
//            MessagesOrLogs.showDebugLog("jsonObjectCampaignInfo======>" + jsonObjectCampaign);

            if (truePushCallbacksInstance != null) {
                truePushCallbacksInstance.onNotificationReceived(jsonObjectNotificationInfo.toString(), "");
            }
            else {
               /* MessagesOrLogs.showDebugLog("truePushCallBacks  is null");

                try {
                    Intent itemintent = null;

                    try {
                        itemintent = new Intent(getApplicationContext(), Class.forName("com.myapps.merchantapp.MainActivity"));//Ex com.example.test.Activity
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bundle b = new Bundle();
                    b.putString("iarray", jsonObjectNotificationInfo.toString());
                    b.putString("action", "action");
                    b.putString("key", "YourString");
                    b.putInt("mflag", 0);
                    itemintent.putExtra("android.intent.extra.INTENT", b);
                    startActivity(itemintent);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }

            String title = jsonObjectNotificationInfo.getString("title");
            String body = jsonObjectNotificationInfo.getString("body");
            String icon = jsonObjectNotificationInfo.getString("icon");
            String imageUrl = jsonObjectNotificationInfo.getString("image");
            link = jsonObjectNotificationInfo.getString("link");
            campaignDataInfo = jsonObjectCampaign.toString();
            JSONArray notificationActions = jsonObjectNotificationInfo.getJSONArray("notificationActions");

            session = new Session(context);
            session.setNotificationMainLink(link);
            session.setNotificationCInfo(campaignDataInfo);
            notificationId = session.getUniqueInt();
            session.setUniqueInt(notificationId+1);
//            notificationId = (int) System.currentTimeMillis();

            if(notificationId == 2147483645){
                notificationId = 0;
            }

            notificationActionsList = new ArrayList<>();
            for (int index = 0; index < notificationActions.length(); index++) {
                JSONObject currentAction = notificationActions.getJSONObject(index);

                String action = currentAction.getString("action");
                String actionTitle = currentAction.getString("title");
                String actionLink = currentAction.getString("link");
                String actionType = currentAction.getString("type");
//                String actionIcon = currentAction.getString("icon");

                Intent notificationIntent = new Intent(context.getApplicationContext(), NotificationEventReceiver.class);
//                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                int uniqueActionDigit = notificationId + index;
                notificationIntent.setAction(actionTitle + notificationId);
                notificationIntent.putExtra("action", action);
                notificationIntent.putExtra("actionTitle", actionTitle + "#" + notificationId);
                notificationIntent.putExtra("actionLink", actionLink);
                notificationIntent.putExtra("actionType", actionType);
                notificationIntent.putExtra("campaignData", jsonObjectCampaign.toString());
                notificationIntent.putExtra("notificationData", jsonObjectNotificationInfo.toString());
                notificationIntent.putExtra("link", link);
//                MessagesOrLogs.showDebugLog("campaignData 1 : " + jsonObjectCampaign.toString());
//                MessagesOrLogs.showDebugLog("notificationId in Service======>" + notificationId);
                notificationIntent.putExtra("notificationId", notificationId);

                PendingIntent contentIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

//                   Bitmap bitmap = getBitmapFromURL(actionIcon);

                NotificationCompat.Action notificationAction = new NotificationCompat.Action(0, actionTitle, contentIntent);

//                MessagesOrLogs.showDebugLog("actionTitle======>" + actionTitle);
                notificationActionsList.add(notificationAction);

            }

            String timestamp = String.valueOf(System.currentTimeMillis());

//            MessagesOrLogs.showDebugLog("timestamp: " + timestamp);

            if (!NotificationUtils.isAppIsInBackground(context.getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Utility.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", body);
//              LocalBroadcastManager.getInstance(context).sendBroadcast(pushNotification);

                setIntentAndSelectNotificationType(context, title, body, icon, imageUrl, timestamp, notificationActionsList, jsonObjectNotificationInfo, jsonObjectCampaign);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(context.getApplicationContext());
//                notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                setIntentAndSelectNotificationType(context, title, body, icon, imageUrl, timestamp, notificationActionsList, jsonObjectNotificationInfo, jsonObjectCampaign);
            }


//            MessagesOrLogs.showDebugLog("campaignData 5 : " + jsonObjectCampaign.toString());
//          Service Call To Update Campaign Stats
            /*ServiceCalls serviceCalls = new ServiceCalls();
            String event = "view";
            String eventType = "default";
            serviceCalls.webServiceCallToUpdateCampaignStats(context.getApplicationContext(), jsonObjectCampaign, event, eventType);*/
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setIntentAndSelectNotificationType(Context context, String title, String message, String icon, String imageUrl,
                                                    String timestamp, List<NotificationCompat.Action> notificationActionsList,
                                                    JSONObject jsonObjectNotificationInfo, JSONObject jsonObjectCampaign) {
//        CommentedIntent
        Intent resultIntent = new Intent(context.getApplicationContext(), NotificationEventReceiver.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
            resultIntent.setAction(title);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("action", context.getString(R.string.action));
            resultIntent.putExtra("actionTitle", title + "#" + notificationId);
            resultIntent.putExtra("actionLink", jsonObjectNotificationInfo.getString("link"));
            resultIntent.putExtra("actionType", context.getString(R.string.action_type));
            resultIntent.putExtra("campaignData", jsonObjectCampaign.toString());
            resultIntent.putExtra("notificationData", jsonObjectNotificationInfo.toString());

//            MessagesOrLogs.showDebugLog("notificationId to Intent 222======>" + notificationId);
//            MessagesOrLogs.showDebugLog("campaignData 2 : " + jsonObjectCampaign.toString());
            resultIntent.putExtra("notificationId", notificationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // check for image attachment
        if (TextUtils.isEmpty(imageUrl)) {
            showNotificationMessage(context.getApplicationContext(), title, message, icon, timestamp, resultIntent, notificationActionsList);
        } else {
            // image is present, show notification with image
            showNotificationMessageWithBigImage(context.getApplicationContext(), title, message, icon, timestamp, resultIntent, imageUrl, notificationActionsList);
        }
    }

    //  Showing notification with text only
    private void showNotificationMessage(Context context, String title, String message, String iconUrl, String timeStamp, Intent intent,
                                         List<NotificationCompat.Action> notificationActionsList) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, iconUrl, "", notificationActionsList, link);
    }

    //  Showing notification with text and image
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String iconUrl, String timeStamp, Intent intent, String imageUrl,
                                                     List<NotificationCompat.Action> notificationActionsList) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, iconUrl, imageUrl, notificationActionsList, link);
    }


}

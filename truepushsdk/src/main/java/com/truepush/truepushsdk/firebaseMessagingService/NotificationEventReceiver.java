package com.truepush.truepushsdk.firebaseMessagingService;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.JsonObject;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.apiServiceCall.ServiceCalls;

import org.json.JSONObject;

import static com.truepush.truepushsdk.TruePush.truePushCallbacksInstance;

public class NotificationEventReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(closeDialog);

        Session session = new Session(context);

        String action = intent.getStringExtra("action");
        String actionTitle = intent.getStringExtra("actionTitle");
        String actionType = intent.getStringExtra("actionType");
        String actionLink = intent.getStringExtra("actionLink");
        String campaignData = intent.getStringExtra("campaignData");
        String notificationData = intent.getStringExtra("notificationData");
        String link = session.getNotificationMainLink();
//        String campaignDataInfo = session.getNotificationCInfo();
        int notificationId = intent.getIntExtra("notificationId", 0);

        if(action == null){
            action = "action";
        }

        /*MessagesOrLogs.showDebugLog("action======>" + action);
        MessagesOrLogs.showDebugLog("actionTitle======>" + actionTitle);
        MessagesOrLogs.showDebugLog("actionType======>" + actionType);
        MessagesOrLogs.showDebugLog("actionLink======>" + actionLink);
        MessagesOrLogs.showDebugLog("notificationId======>" + notificationId);
        MessagesOrLogs.showDebugLog("link======>" + link);
        MessagesOrLogs.showDebugLog("campaignData======>" + campaignData);
        MessagesOrLogs.showDebugLog("notificationData======>" + notificationData);*/
//        MessagesOrLogs.showDebugLog("campaignDataInfo======>" + campaignDataInfo);

        if (truePushCallbacksInstance != null)
            truePushCallbacksInstance.onNotificationReceived(notificationData, action);
        else{
            System.out.println("truePushCallbacksInstance is null");
        }
        ServiceCalls serviceCalls = new ServiceCalls();

        String event = intent.hasExtra("action") ? action : "click";   //View
        String eventType;
        if (event.equalsIgnoreCase("click")) {
            eventType = "default";
        } else {
            eventType = intent.hasExtra("action") ? "action" : "default";  //Default
        }
        try {
            new JsonObject();
            if (campaignData != null) {
                serviceCalls.webServiceCallToUpdateCampaignStats(context, new JSONObject(campaignData), event, eventType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(actionTitle)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Integer.parseInt(actionTitle.split("#")[1]));
            notificationManager.cancel(notificationId);
        }

    }
}

package com.truepush.truepushsdk.firebaseMessagingService;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.truepush.truepushsdk.R;
import com.truepush.truepushsdk.Session.Session;
import com.truepush.truepushsdk.apiServiceCall.ServiceCalls;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class NotificationUtils {

    private Context mContext;
    public int notificationId;
    private String fullNotificationLink;
    private String NOTIFICATION_CHANNEL_ID = "TruePush";
    private int UNIQUE_INT_PER_CALL = 0;
    private Bitmap iconBitmapNew;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void showNotificationMessage(final String title, final String message, final String timeStamp, final Intent intent,
                                        final String iconUrl, final String imageUrl, final List<NotificationCompat.Action> notificationActionsList, String link) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

//      notification icon
//      cast_ic_notification_small_icon replaced with cast_ic_notification_small_icon
        final int icon = R.drawable.action_icon;
        this.fullNotificationLink = link;
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getBroadcast(
                        mContext,
                        UNIQUE_INT_PER_CALL,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

//        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//                mContext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "TruePush", importance);
            mChannel.setDescription("TruePush");
            mChannel.setShowBadge(true);
            mChannel.canShowBadge();
            mChannel.enableLights(true);

            mChannel.setLightColor(ContextCompat.getColor(mContext.getApplicationContext(), R.color.blue_light));
            NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(mChannel);
        }

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID);
        mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);

        /*final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + mContext.getPackageName() + "/raw/notification");*/

        final Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        final Bitmap iconBitmap = getBitmapFromURL(iconUrl);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    //MessagesOrLogs.showDebugInfo(" iconUrl : " + iconUrl);
                    if(iconUrl != null && iconUrl.length() > 0){
                        URL url = new URL(iconUrl);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        iconBitmapNew = BitmapFactory.decodeStream(input);
                        /*URL url = new URL(iconUrl);
                        iconBitmapNew = BitmapFactory.decodeStream(url.openConnection().getInputStream());*/

                    }
                    System.out.println("Imageurl -"+ imageUrl);
                    if (!TextUtils.isEmpty(imageUrl)) {

                        if (imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

//                          Bitmap bitmap = getBitmapFromURL(imageUrl);
                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Bitmap myBitmap = null;
                                        //MessagesOrLogs.showDebugInfo(" imageUrl : " + imageUrl);
                                        if(imageUrl != null && imageUrl.length() > 0) {
                                            URL url = new URL(imageUrl);
                                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                            connection.setDoInput(true);
                                            connection.connect();
                                            InputStream input = connection.getInputStream();
                                            myBitmap = BitmapFactory.decodeStream(input);
                                            /*URL url = new URL(iconUrl);
                                            myBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());*/
                                        }


                                        if (myBitmap != null) {
                                            showCustomBigNotification(myBitmap, mBuilder, icon, title, message, timeStamp,
                                                    intent, alarmSound, notificationActionsList, iconBitmapNew);
                                            //showBigNotification(title, message, imageUrl);
                                            System.out.println("Imageurl -"+ imageUrl + " Bitmap "+ myBitmap);
                                           /* new generatePictureStyleNotification(mContext, title, message,
                                                    imageUrl).execute();*/
                                            playNotificationSound();
                                        } else {
                                            showCustomSmallNotification(mBuilder, icon, title, message, timeStamp,
                                                    intent, alarmSound, notificationActionsList, iconBitmapNew);
                                            playNotificationSound();
                                        }

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


                        }
                    } else {
                        showCustomSmallNotification(mBuilder, icon, title, message, timeStamp,
                                intent, alarmSound, notificationActionsList, iconBitmapNew);
                        playNotificationSound();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void showCustomSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, Intent intent,
                                             Uri alarmSound, List<NotificationCompat.Action> notificationActionsList, Bitmap iconBitmap) {

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

//        CommentedIntent
        Intent notificationIntent = new Intent(mContext, NotificationEventReceiver.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("message", message);
        notificationIntent.putExtra("campaignData", intent.getStringExtra("campaignData"));
        notificationIntent.putExtra("notificationData", intent.getStringExtra("notificationData"));
        notificationIntent.putExtra("link", fullNotificationLink);

        Session session = new Session(mContext);
        /*UNIQUE_INT_PER_CALL = session.getUniqueInt();

        if(UNIQUE_INT_PER_CALL == 2147483645){
            UNIQUE_INT_PER_CALL = 0;
        }*/
        notificationId = intent.getIntExtra("notificationId", 0);
//        PendingIntent contentIntent = PendingIntent.getBroadcast(mContext, UNIQUE_INT_PER_CALL, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent contentIntent = PendingIntent.getBroadcast(mContext, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(message);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(session.getNotificationDrawableIcon())
                .setContentTitle(title)
                .setShowWhen(false)
                .setContentText(message)
                .setContentIntent(contentIntent)
                .setLargeIcon(iconBitmap)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setStyle(bigTextStyle)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(Notification.PRIORITY_HIGH)
                .setGroup(Integer.toString(notificationId));
        notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);

        Iterator<NotificationCompat.Action> iteratorObject = notificationActionsList.iterator();
        while (iteratorObject.hasNext()) {
            notificationBuilder.addAction(iteratorObject.next());
        }

//        notificationBuilder.setContentIntent(contentIntent);
        notificationManager.notify(notificationId, notificationBuilder.build());
//        session.setUniqueInt(UNIQUE_INT_PER_CALL+1);

        try {

            JSONObject obj = new JSONObject(intent.getStringExtra("campaignData"));
            ServiceCalls serviceCalls = new ServiceCalls();
            String event = "view";
            String eventType = "default";
            serviceCalls.webServiceCallToUpdateCampaignStats(mContext.getApplicationContext(), obj, event, eventType);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void showCustomBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, Intent intent,
                                           Uri alarmSound, List<NotificationCompat.Action> notificationActionsList, Bitmap iconBitmap) {

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

//        CommentedIntent
        Intent notificationIntent = new Intent(mContext, NotificationEventReceiver.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("message", message);
        notificationIntent.putExtra("campaignData", intent.getStringExtra("campaignData"));
        notificationIntent.putExtra("notificationData", intent.getStringExtra("notificationData"));
        notificationIntent.putExtra("link", fullNotificationLink);

        Session session = new Session(mContext);
       /* UNIQUE_INT_PER_CALL = session.getUniqueInt();
        if(UNIQUE_INT_PER_CALL == 2147483645){
            UNIQUE_INT_PER_CALL = 0;
        }*/
        notificationId = intent.getIntExtra("notificationId", 0);
//        PendingIntent contentIntent = PendingIntent.getBroadcast(mContext, UNIQUE_INT_PER_CALL, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent contentIntent = PendingIntent.getBroadcast(mContext, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(message);

        System.out.println("This is the big notification");
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(session.getNotificationDrawableIcon())
                .setContentTitle(title)
                .setShowWhen(false)
                .setContentText(message)
                .setContentIntent(contentIntent)
                .setLargeIcon(iconBitmap)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
//                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                        .bigLargeIcon(null))
//                .setStyle(bigTextStyle)
                .setPriority(Notification.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setGroup(Integer.toString(notificationId))
                .setChannelId(NOTIFICATION_CHANNEL_ID);

      /*  final Notification notifyDetails = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.action_icon)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap))
                .setContentIntent(contentIntent)
                .addAction(R.drawable.action_icon, "Share", contentIntent)
                .build();
        notificationManager.notify(notificationId, notifyDetails);*/

        //notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);

        Iterator<NotificationCompat.Action> iteratorObject = notificationActionsList.iterator();
        while (iteratorObject.hasNext()) {
            notificationBuilder.addAction(iteratorObject.next());
        }

        notificationManager.notify(notificationId, notificationBuilder.build());
//        session.setUniqueInt(UNIQUE_INT_PER_CALL+1);


        try {

            JSONObject obj = new JSONObject(intent.getStringExtra("campaignData"));
            ServiceCalls serviceCalls = new ServiceCalls();
            String event = "view";
            String eventType = "default";
            serviceCalls.webServiceCallToUpdateCampaignStats(mContext.getApplicationContext(), obj, event, eventType);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {

        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showBigNotification(String title, String message, String url) {
        Bitmap bitmap = getBitmapFromURL(url);

        Notification.BigPictureStyle bigPictureStyle = new Notification.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(message);
        bigPictureStyle.bigPicture(bitmap);

        Intent notificationIntent = new Intent(mContext, NotificationEventReceiver.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mContext,
                101, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = mContext.getResources();
        Notification.Builder builder = new Notification.Builder(mContext);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.action_icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.action_icon))
                .setTicker(res.getString(R.string.app_name))
                .setStyle(bigPictureStyle)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(message);
        Notification n = builder.build();

        if (nm != null) {
            nm.notify(notificationId, n);
        }
    }


    // Playing notification sound
    public void playNotificationSound() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            try {
            /*Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");*/
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
                r.play();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private static String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
        String formattedDate = dateFormat.format(new Date()).toString();

        return formattedDate;
    }

    public static class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;

        public generatePictureStyleNotification(Context context, String title, String message, String imageUrl) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
         //   System.out.println("generatePictureStyleNotification onPost "+ result);
            Intent intent = new Intent(mContext, NotificationEventReceiver.class);
            intent.putExtra("key", "value");
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notif = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.action_icon)
                    .setLargeIcon(result)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(result))
                    .build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(1, notif);
        }
    }

}



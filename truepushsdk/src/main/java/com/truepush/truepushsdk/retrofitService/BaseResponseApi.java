package com.truepush.truepushsdk.retrofitService;

import com.truepush.truepushsdk.models.AddUser;
import com.truepush.truepushsdk.models.GetTags;
import com.truepush.truepushsdk.models.Subcriber;
import com.truepush.truepushsdk.models.TagData;
import com.truepush.truepushsdk.models.UpdateVisit;

import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class BaseResponseApi extends RetrofitClient {

    public interface ApiInterface {

      //  @FormUrlEncoded
      //  @Headers("Content-Type: application/json")
      @Headers("Content-type: application/json")
        @POST(ApiEndPoints.UPDATE_VISIT_STATS)
        Call<UpdateVisit> updateVisit(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
     //   @Headers("Content-Type: application/json")
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.POST_DEVICE_INFO)
        Call<Subcriber> postDeviceID(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.GET_SUBSCRIBER_ID)
        Call<Subcriber> getSubscriber(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.ADD_TAGS)
        Call<TagData> add_tags(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.GET_TAGS)
        Call<GetTags> get_tag(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.REMOVE_TAGS)
        Call<TagData> remove_tag(@Body RequestBody jsonObject);

     //   @FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.UPDATE_CAMPAIGN_STATS)
        Call<Subcriber> campaignStatus(@Body RequestBody jsonObject);

        /*@FormUrlEncoded
        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.ADD_USER_ID)
        Call<AddUser> add_user(JSONObject jsonObject);*/

     //   @FormUrlEncoded
/*        @Headers("Content-Type: application/json")
        @POST(ApiEndPoints.ADD_TEST_USER)
        Call<AddUser> add_test_user(@Body JSONObject jsonObject);*/

    }

    public static ApiInterface getApiService() {
        return getClient().create(ApiInterface.class);
    }
}

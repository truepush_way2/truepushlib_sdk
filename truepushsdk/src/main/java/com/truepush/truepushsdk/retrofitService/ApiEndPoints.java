package com.truepush.truepushsdk.retrofitService;

public class ApiEndPoints {

    public static final String SERVICE_VERSION = "v1";
//    public static final String BASE_URL = "http://6da68fe1a68e.ngrok.io/android/api/v1/"; // pruviraj
    public static final String BASE_URL = "https://lab.truepush.com/android/api/v1/"; // pruviraj

    static final String POST_DEVICE_INFO = BASE_URL + "addSubscriber";
    static final String ADD_TAGS = BASE_URL + "addTags";
    static final String REMOVE_TAGS = BASE_URL + "removeTags";
    static final String GET_TAGS = BASE_URL + "getTags";
    static final String UPDATE_CAMPAIGN_STATS = BASE_URL + "updateCampaignStats";
    static final String UPDATE_VISIT_STATS = BASE_URL + "updateVisit";
    static final String GET_SUBSCRIBER_ID = BASE_URL + "getSubscriberId";
//    static final String GET_SENDER_ID = BASE_URL + "getSenderId";
//    static final String ADD_USER_ID = BASE_URL + "addTags";
//    static final String ADD_TEST_USER = BASE_URL + "addTestUser";
}
